﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
public class ScriptsCamera : MonoBehaviour
{
    private WebCamTexture camDevice;
    public RawImage background;
    public AspectRatioFitter fit;
    public Button btn;
    private bool isFrontFacing = false;
    private const int SIZE_RESOLUSTION = 256;
    private AndroidJavaObject hand_tracking;
    private HandRect current_hand_rect;
    private void Start()
    {
        KeepAwake();
        this.btn.onClick.AddListener(OnClick);

        while (!IsCamAvailable()) ;
        SetCam();
        this.background.texture = this.camDevice;
        this.background.material.mainTexture = this.camDevice;
        this.camDevice.Play();

        InitHandTracking();

        StartCoroutine(Quantization());
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        // float ratio = (float)this.camDevice.width / (float)this.camDevice.height;
        // fit.aspectRatio = ratio;

        // if (isFrontFacing)
        // {
        //     float scaleY = this.camDevice.videoVerticallyMirrored ? -1f : 1f;
        //     this.background.rectTransform.localScale = new Vector3(-1f, scaleY, 1f);

        //     int orient = -this.camDevice.videoRotationAngle;
        //     this.background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        // }
    }

    private void KeepAwake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private bool IsCamAvailable()
    {
        return WebCamTexture.devices.Length != 0;
    }

    private void SetCam()
    {
        foreach (var camDevice in WebCamTexture.devices)
            if (camDevice.isFrontFacing == this.isFrontFacing)
            {
                Debug.Log(camDevice.name);
                this.camDevice = new WebCamTexture(camDevice.name);
            }
    }

    private void OnClick()
    {
        // Texture2D ImageConversion = new Texture2D(this.camDevice.width, this.camDevice.height);

        // ImageConversion.SetPixels32(this.camDevice.GetPixels32());
        // ImageConversion.Apply(false);
        // byte[] frameImage = ImageConversion.EncodeToJPG();
        // sbyte[] frameImageSigned = Array.ConvertAll(frameImage, b => unchecked((sbyte)b));

        // Debug.Log("setFrame");
        // hand_tracking.Call("setFrame", frameImageSigned);

        float[] palm_data = hand_tracking.Call<float[]>("getPalmRect");
        Debug.Log("getPalmRect ");
        float[] hand_landmarks_data = hand_tracking.Call<float[]>("getLandmarks");
        Debug.Log("getLandmarks ");

        if (palm_data != null)
        {
            current_hand_rect = HandRect.ParseFrom(palm_data);
            string text = "hand rect: ("
                + "XCenter:" + current_hand_rect.XCenter + ", "
                + "YCenter:" + current_hand_rect.YCenter + ", "
                + "Height:" + current_hand_rect.Height + ", "
                + "Width:" + current_hand_rect.Width + ", "
                + "Rotaion:" + current_hand_rect.Rotaion + ")";
            Debug.Log(text);
        }
    }

    private void InitHandTracking()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentUnityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        hand_tracking = new AndroidJavaObject("com.jackie.mediapipe.HandTracking", currentUnityActivity);
        hand_tracking.Call("setResolution", SIZE_RESOLUSTION);
        Debug.Log("setResolution ");
    }

    public IEnumerator Quantization()
    {
        while (true)
        {
            Debug.Log("Quantization");
            yield return new WaitForEndOfFrame();
            hand_tracking.Call("setFrame", this.camDevice.GetPixels32());
            Debug.Log("setFrame");
            yield return null;
        }
    }
}